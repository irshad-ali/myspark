//
//  TrackUsageConnabinoidsVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 08/12/21.
//

import UIKit

class TrackUsageConnabinoidsVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    
    @IBOutlet weak var View1: UIView!
    
    @IBOutlet weak var View2: UIView!
    
    
    @IBOutlet weak var View3: UIView!
    
    @IBOutlet weak var View4: UIView!
   
    @IBOutlet weak var View5: UIView!
 
    @IBOutlet weak var View6: UIView!
    
    @IBOutlet weak var View7: UIView!
    
    @IBOutlet weak var View8: UIView!
    
    @IBOutlet weak var View9: UIView!
    
    @IBOutlet weak var View10: UIView!
    
    @IBOutlet weak var View11: UIView!
    
    @IBOutlet weak var View12: UIView!
    
    
    @IBOutlet weak var txt1: UITextField!
    
    @IBOutlet weak var txt2: UITextField!
    
    @IBOutlet weak var txt3: UITextField!
    
    @IBOutlet weak var txt4: UITextView!
    
    @IBOutlet weak var txt5: UITextField!
    
    @IBOutlet weak var txt6: UITextField!
    
    @IBOutlet weak var txt7: UITextView!
    
    
    @IBOutlet weak var TblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//       
//        TblView.dataSource = self
//        TblView.delegate = self

        txt1.delegate = self
        txt2.delegate = self
        txt3.delegate = self
        txt4.delegate = self
        txt5.delegate = self
//        txt6.delegate = self
        txt7.delegate = self

       
        View1.layer.borderWidth = 1
        View1.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
         View2.layer.borderWidth = 1
         View2.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View3.layer.borderWidth = 1
         View3.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View4.layer.borderWidth = 1
         View4.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View5.layer.borderWidth = 1
         View5.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View6.layer.borderWidth = 1
         View6.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View7.layer.borderWidth = 1
         View7.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View8.layer.borderWidth = 1
         View8.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View9.layer.borderWidth = 1
         View9.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View10.layer.borderWidth = 1
         View10.layer.borderColor = UIColor.lightGray.cgColor
         
        
         View11.layer.borderWidth = 1
         View11.layer.borderColor = UIColor.lightGray.cgColor
         
        
        
       
        
    }
    

    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txt1.resignFirstResponder()
        txt2.resignFirstResponder()
        txt3.resignFirstResponder()
        txt4.resignFirstResponder()
        txt5.resignFirstResponder()
//        txt6.resignFirstResponder()
        txt7.resignFirstResponder()
        
        
        
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n"){
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}

   



//extension TrackUsageConnabinoidsVC: UITableViewDelegate, UITableViewDataSource{
//
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        TblView.separatorStyle = .none
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackUsageConnabinoidsTblVCell") as! TrackUsageConnabinoidsTblVCell
//        cell.ViewDate.layer.borderWidth = 1
//        cell.ViewDate.layer.borderColor = UIColor .lightGray.cgColor
//
//        cell.ViewTime.layer.borderWidth = 1
//        cell.ViewTime.layer.borderColor = UIColor .lightGray.cgColor
//
//        cell.ViewProductID.layer.borderWidth = 0.5
//        cell.ViewProductID.layer.borderColor = UIColor.lightGray.cgColor
//
//        cell.ViewLotNumber.layer.borderWidth = 1
//        cell.ViewLotNumber.layer.borderColor = UIColor.lightGray.cgColor
//
//        cell.ViewProductName.layer.borderWidth = 1
//        cell.ViewProductName.layer.borderColor = UIColor .lightGray.cgColor
//        cell.ViewTextFieldProductDcription.layer.borderWidth = 1
//        cell.ViewTextFieldProductDcription.layer.borderColor = UIColor.lightGray.cgColor
//        cell.ViewMethod.layer.borderWidth = 1
//        cell.ViewMethod.layer.borderColor = UIColor.lightGray.cgColor
//        cell.ViewQuantity.layer.borderWidth = 1
//        cell.ViewQuantity.layer.borderColor = UIColor.lightGray.cgColor
//        cell.ViewQuantityUnit.layer.borderWidth = 1
//        cell.ViewQuantityUnit.layer.borderColor = UIColor.lightGray.cgColor
//        cell.ViewTHCMg.layer.borderWidth = 1
//        cell.ViewTHCMg.layer.borderColor = UIColor.lightGray.cgColor
//        cell.ViewCBDmg.layer.borderWidth = 1
//        cell.ViewCBDmg.layer.borderColor = UIColor.lightGray.cgColor
//        cell.ViewNotesFields.layer.borderWidth = 1
//        cell.ViewNotesFields.layer.borderColor = UIColor.lightGray.cgColor
//
//        cell.txt1.resignFirstResponder()
//
//
//        return cell
//    }
//
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        800
//    }
//
//
//}
