//
//  TrackUsageConnabinoidsTblVCell.swift
//  My Spark
//
//  Created by YATIN  KALRA on 08/12/21.
//

import UIKit

class TrackUsageConnabinoidsTblVCell: UITableViewCell {

    @IBOutlet weak var ViewDate: UIView!
    @IBOutlet weak var ViewTime: UIView!
    @IBOutlet weak var ViewProductID: UIView!
    @IBOutlet weak var ViewLotNumber: UIView!
    @IBOutlet weak var ViewProductName: UIView!
    @IBOutlet weak var ViewTextFieldProductDcription: UIView!
    @IBOutlet weak var ViewMethod: UIView!
    @IBOutlet weak var ViewQuantity: UIView!
    @IBOutlet weak var ViewQuantityUnit: UIView!
    @IBOutlet weak var ViewTHCMg: UIView!
    @IBOutlet weak var ViewCBDmg: UIView!
    @IBOutlet weak var ViewNotesFields: UIView!
    
    @IBOutlet weak var txt1: UITextField!
    
    @IBOutlet weak var txt2: UITextField!
    
    @IBOutlet weak var txt3: UITextField!
    
    @IBOutlet weak var txt4: UITextView!
  
    @IBOutlet weak var txt5: UITextField!
    
    @IBOutlet weak var txt6: UITextView!
}
