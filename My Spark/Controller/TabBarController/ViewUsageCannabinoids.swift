//
//  ViewUsageCannabinoids.swift
//  My Spark
//
//  Created by YATIN  KALRA on 09/12/21.
//

import UIKit

class ViewUsageCannabinoids: UIViewController {

    @IBOutlet weak var TblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TblView.delegate = self
        TblView.dataSource = self

    }
    
    
    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension ViewUsageCannabinoids: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        TblView.separatorStyle = .none
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ViewUsageCannabinoidsTblViewCell
        
        cell.ViewDate.layer.borderWidth = 1
        cell.ViewDate.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewTime.layer.borderWidth = 1
        cell.ViewTime.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewProductID.layer.borderWidth = 1
        cell.ViewProductID.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewLotNumber.layer.borderWidth = 1
        cell.ViewLotNumber.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewProductName.layer.borderWidth = 1
        cell.ViewProductName.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewProductDescription.layer.borderWidth = 1
        cell.ViewProductDescription.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewMethod.layer.borderWidth = 1
        cell.ViewMethod.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewQuantity.layer.borderWidth = 1
        cell.ViewQuantity.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewQuantityUnit.layer.borderWidth = 1
        cell.ViewQuantityUnit.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewThcmg.layer.borderWidth = 1
        cell.ViewThcmg.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewCBDmg.layer.borderWidth = 1
        cell.ViewCBDmg.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewNotesTxtField.layer.borderWidth = 1
        cell.ViewNotesTxtField.layer.borderColor = UIColor.lightGray.cgColor
        
    
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        800
    }
 
    
    
    
}
