//
//  TrackUsageConnabinoidsVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 08/12/21.
//

import UIKit

class TrackUsageConnabinoidsVC: UIViewController {
    
    
    @IBOutlet weak var TblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        TblView.dataSource = self
        TblView.delegate = self

       
    }
    

    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}


extension TrackUsageConnabinoidsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        TblView.separatorStyle = .none
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackUsageConnabinoidsTblVCell") as! TrackUsageConnabinoidsTblVCell
        cell.ViewDate.layer.borderWidth = 1
        cell.ViewDate.layer.borderColor = UIColor .lightGray.cgColor
        
        cell.ViewTime.layer.borderWidth = 1
        cell.ViewTime.layer.borderColor = UIColor .lightGray.cgColor
        
        cell.ViewProductID.layer.borderWidth = 0.5
        cell.ViewProductID.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewLotNumber.layer.borderWidth = 1
        cell.ViewLotNumber.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewProductName.layer.borderWidth = 1
        cell.ViewProductName.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewTextFieldProductDcription.layer.borderWidth = 1
        cell.ViewTextFieldProductDcription.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ViewMethod.layer.borderWidth = 1
        cell.ViewMethod.layer.borderColor = UIColor.lightGray.cgColor
        cell.ViewQuantity.layer.borderWidth = 1
        cell.ViewQuantity.layer.borderColor = UIColor.lightGray.cgColor
        cell.ViewQuantityUnit.layer.borderWidth = 1
        cell.ViewQuantityUnit.layer.borderColor = UIColor.lightGray.cgColor
        cell.ViewTHCMg.layer.borderWidth = 1
        cell.ViewTHCMg.layer.borderColor = UIColor.lightGray.cgColor
        cell.ViewCBDmg.layer.borderWidth = 1
        cell.ViewCBDmg.layer.borderColor = UIColor.lightGray.cgColor
        cell.ViewNotesFields.layer.borderWidth = 1
        cell.ViewNotesFields.layer.borderColor = UIColor.lightGray.cgColor
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        800
    }
 
    
}
