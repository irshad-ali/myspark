//
//  TrackUsageVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 08/12/21.
//

import UIKit

class TrackUsageVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    
    
    @IBOutlet weak var Txt1: UITextField!
    
    
    @IBOutlet weak var Txt2: UITextField!
    
    @IBOutlet weak var Txt3: UITextView!
    
    @IBOutlet weak var ViewDate: UIView!
    
    @IBOutlet weak var Viewtime: UIView!
    
    
    @IBOutlet weak var ViewDin: UIView!
    
    
    @IBOutlet weak var ViewProductName: UIView!
    
    
    @IBOutlet weak var ViewQuantityUnit: UIView!
    
    @IBOutlet weak var ViewQuentety: UIView!
    
    @IBOutlet weak var ViewNoteTextField: UIView!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var UiviewDate: UIView!
    
    @IBOutlet weak var UIviewTime: UIView!
    
    @IBOutlet weak var BtnColender: UIButton!
    
    @IBOutlet weak var LblTime: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        Txt1.delegate = self
        Txt2.delegate = self
        Txt3.delegate = self
        
        ViewDate.layer.borderColor = UIColor .lightGray .cgColor
        ViewDate.layer.borderWidth = 1
        
        Viewtime.layer.borderColor = UIColor .lightGray .cgColor
        Viewtime.layer.borderWidth = 1

        
        ViewDin.layer.borderColor = UIColor .lightGray .cgColor
        ViewDin.layer.borderWidth = 1

        
        ViewProductName.layer.borderColor = UIColor .lightGray .cgColor
        ViewProductName.layer.borderWidth = 1

        
        
        ViewQuentety.layer.borderColor = UIColor .lightGray .cgColor
        ViewQuentety.layer.borderWidth = 1
        
        ViewQuantityUnit.layer.borderColor = UIColor .lightGray .cgColor
        ViewQuantityUnit.layer.borderWidth = 1

        
        ViewNoteTextField.layer.borderColor = UIColor .lightGray .cgColor
        ViewNoteTextField.layer.borderWidth = 1

        
        ViewDate.layer.borderColor = UIColor .lightGray .cgColor
        ViewDate.layer.borderWidth = 1

        
       


        
    }
    

    @IBAction func BtnTime(_ sender: UIButton) {
    }
    
    @IBAction func BtnDate(_ sender: UIButton) {
    }
    
    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Txt1.resignFirstResponder()
        Txt2.resignFirstResponder()
        
        return true
    }
    
//    func textViewShouldReturn(textView: UITextView!) -> Bool {
//            self.view.endEditing(true);
//        Txt2.resignFirstResponder()
//            return true;
//        }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n"){
            textView.resignFirstResponder()
            return false
        }
        return true
    }


}
