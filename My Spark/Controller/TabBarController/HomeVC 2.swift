//
//  HomeVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 07/12/21.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var uiview: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
//        uiview.layer.cornerRadius = 10
//        uiview.layer.masksToBounds = true

       
    }
    
    @IBAction func BtnCannRX(_ sender: AnyObject) {
        if let url = NSURL(string: "https://naturalhealthrx.ca/"){
            UIApplication.shared.openURL(url as URL)
            }
    }
    
    
    @IBAction func BtnCannTracker(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CannTrackerVC") as! CannTrackerVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func BtnBioTracker(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BioTrackerVC") as! BioTrackerVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func BtnMedTracker(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "madTrackerVC") as! madTrackerVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
