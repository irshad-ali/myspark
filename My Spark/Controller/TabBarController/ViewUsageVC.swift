//
//  ViewUsageVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 08/12/21.
//

import UIKit

class ViewUsageVC: UIViewController {
    
    
    @IBOutlet weak var ViewDate: UIView!
    
    
   
    @IBOutlet weak var ViewTime: UIView!
    
    @IBOutlet weak var ViewDin: UIView!
    
    @IBOutlet weak var ViewProductName: UIView!
    
    @IBOutlet weak var ViewQuentity: UIView!
    
    
    @IBOutlet weak var ViewQuentityUnit: UIView!
    
    
    @IBOutlet weak var ViewNoteField: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ViewTime.layer.borderWidth = 1
        ViewTime.layer.borderColor = UIColor .lightGray .cgColor

        
        ViewDate.layer.borderWidth = 1
        ViewDate.layer.borderColor = UIColor .lightGray .cgColor
        
        ViewDin.layer.borderWidth = 1
        ViewDin.layer.borderColor = UIColor .lightGray .cgColor
        
        ViewProductName.layer.borderWidth = 1
        ViewProductName.layer.borderColor = UIColor .lightGray .cgColor
        
        ViewQuentity.layer.borderWidth = 1
        ViewQuentity.layer.borderColor = UIColor .lightGray .cgColor
        
        ViewQuentityUnit.layer.borderWidth = 1
        ViewQuentityUnit.layer.borderColor = UIColor .lightGray .cgColor
        
        ViewNoteField.layer.borderWidth = 1
        ViewNoteField.layer.borderColor = UIColor .lightGray .cgColor
        
       
        
    }
    
    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    

}
