//
//  madTrackerVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 08/12/21.
//

import UIKit

class madTrackerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    @IBAction func BtnMedications(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "medtrackerMedicationsVC") as! medtrackerMedicationsVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func BtnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
