//
//  CannTrackerVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 08/12/21.
//

import UIKit

class CannTrackerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    

    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func BtnTrackUsageVC(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TrackUsageConnabinoidsVC") as! TrackUsageConnabinoidsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func BtnViewUsageVC(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewUsageCannabinoids") as! ViewUsageCannabinoids
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
