//
//  ViewUsageCannabinoidsTblViewCell.swift
//  My Spark
//
//  Created by YATIN  KALRA on 09/12/21.
//

import UIKit

class ViewUsageCannabinoidsTblViewCell: UITableViewCell {
    
    @IBOutlet weak var ViewDate: UIView!
    @IBOutlet weak var ViewTime: UIView!
    @IBOutlet weak var ViewProductID: UIView!
    @IBOutlet weak var ViewLotNumber: UIView!
    @IBOutlet weak var ViewProductDescription: UIView!
    @IBOutlet weak var ViewMethod: UIView!
    @IBOutlet weak var ViewQuantity: UIView!
    @IBOutlet weak var ViewQuantityUnit: UIView!
    @IBOutlet weak var ViewThcmg: UIView!
    @IBOutlet weak var ViewCBDmg: UIView!
    @IBOutlet weak var ViewNotesTxtField: UIView!
    
    @IBOutlet weak var ViewProductName: UIView!
    
    
}
