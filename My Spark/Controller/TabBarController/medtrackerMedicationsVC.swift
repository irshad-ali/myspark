//
//  medtrackerMedicationsVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 08/12/21.
//

import UIKit

@available(iOS 13.0, *)
class medtrackerMedicationsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        overrideUserInterfaceStyle = .dark
        
    }
    

    @IBAction func BtnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

    
    @IBAction func BtnTrackUsage(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "TrackUsageVC") as! TrackUsageVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func BtnViewUsage(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewUsageVC") as! ViewUsageVC

        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
