//
//  verificationSaveChangeVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 06/12/21.
//

import UIKit

class verificationSaveChangeVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var View1: UIView!
    
    @IBOutlet weak var View2: UIView!
    
    @IBOutlet weak var View3: UIView!
    
    
    @IBOutlet weak var View4: UIView!
    
    @IBOutlet weak var View5: UIView!
    
    
    @IBOutlet weak var TxtName: UITextField!
    @IBOutlet weak var TxtGender: UITextField!
    @IBOutlet weak var TxtDateOFBirth: UITextField!
    @IBOutlet weak var TxtPhone: UITextField!
    @IBOutlet weak var TxtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TxtName.delegate = self
        TxtGender.delegate = self
        TxtDateOFBirth.delegate = self
        TxtPhone.delegate = self
        TxtEmail.delegate = self
        
        
        View1.layer.cornerRadius = 15
        View1.layer.borderColor = UIColor .lightGray .cgColor

        
        View2.layer.cornerRadius = 15
        View2.layer.borderColor = UIColor .lightGray .cgColor

        View3.layer.cornerRadius = 15
        View3.layer.borderColor = UIColor .lightGray .cgColor

        View4.layer.cornerRadius = 15
        View4.layer.borderColor = UIColor .lightGray .cgColor

        View5.layer.cornerRadius = 15
        View5.layer.borderColor = UIColor .lightGray .cgColor

        
    }
    

    @IBAction func BtnSaveChanges(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "TabBarVController") as! TabBarVController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        TxtName.resignFirstResponder()
        TxtGender.resignFirstResponder()
        TxtDateOFBirth.resignFirstResponder()
        TxtPhone.resignFirstResponder()
        TxtEmail.resignFirstResponder()
        
        
        return true
    }
    func displayAllSubviews(for view: UIView) {
        view.subviews.forEach { subview in
            print(subview.debugDescription)
            displayAllSubviews(for: subview)
        }
    }
}
