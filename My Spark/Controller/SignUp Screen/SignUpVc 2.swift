//
//  SignUpVc.swift
//  My Spark
//
//  Created by YATIN  KALRA on 06/12/21.
//

import UIKit
import IQKeyboardManagerSwift

class SignUpVc: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var LblSignUp: UILabel!
    
    @IBOutlet weak var LblName: UITextField!
    
    @IBOutlet weak var LblEmail: UITextField!
    @IBOutlet weak var LblPhoneNumber: UITextField!
    
    @IBOutlet weak var LblPassword: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.LblName.delegate = self
        self.LblEmail.delegate = self
        self.LblPhoneNumber.delegate = self
        self.LblPassword.delegate = self
        

        
    }
    
    @IBAction func BtnSignin(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    @IBAction func BtnSignUp(_ sender: UIButton) {
//        if validationOfTextFields() == true {
        let vc = storyboard?.instantiateViewController(withIdentifier: "VerificationCodeVC") as! VerificationCodeVC
        self.navigationController?.pushViewController(vc, animated: true)
//    }
    }
    
    
    
    
    
    
    // MARK: - Check Validation Method
    
    func validationOfTextFields() -> Bool{
        
        if LblName.text == "" {
            self.showOkAlertWithHandler(AlertMessages.userName.rawValue) {
            }
            return false
        }
        else if LblEmail.text == "" {
            self.showOkAlertWithHandler(AlertMessages.Enterregisteredemail.rawValue) {
            }
            return false
        }
        
        else if LblPassword.text == "" {
            self.showOkAlertWithHandler(AlertMessages.enterPassword.rawValue) {
            }
            return false
        }
        else if (LblPassword?.text?.count)! < 6 {
            self.showOkAlertWithHandler(AlertMessages.passwordcount.rawValue) {
            }
            return false
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        LblName.resignFirstResponder()
        LblEmail.resignFirstResponder()
        LblPhoneNumber.resignFirstResponder()
        LblPassword.resignFirstResponder()
        return true
    }
    
    
    
}
