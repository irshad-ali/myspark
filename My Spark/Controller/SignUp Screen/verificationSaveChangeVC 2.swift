//
//  verificationSaveChangeVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 06/12/21.
//

import UIKit

class verificationSaveChangeVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var TxtName: UITextField!
    @IBOutlet weak var TxtGender: UITextField!
    @IBOutlet weak var TxtDateOFBirth: UITextField!
    @IBOutlet weak var TxtPhone: UITextField!
    @IBOutlet weak var TxtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TxtName.delegate = self
        TxtGender.delegate = self
        TxtDateOFBirth.delegate = self
        TxtPhone.delegate = self
        TxtEmail.delegate = self
        

        
    }
    

    @IBAction func BtnSaveChanges(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "TabBarVController") as! TabBarVController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func BtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        TxtName.resignFirstResponder()
        TxtGender.resignFirstResponder()
        TxtDateOFBirth.resignFirstResponder()
        TxtPhone.resignFirstResponder()
        TxtEmail.resignFirstResponder()
        
        
        return true
    }
    
}
