//
//  ForgotpasswordSendVerificationEmailVc.swift
//  My Spark
//
//  Created by YATIN  KALRA on 07/12/21.
//

import UIKit

class ForgotpasswordSendVerificationEmailVc: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txt1: UITextField!
    
    @IBOutlet weak var txt2: UITextField!
    
    @IBOutlet weak var txt3: UITextField!
    
    @IBOutlet weak var txt4: UITextField!
    
    @IBOutlet weak var txt5: UITextField!
    
    @IBOutlet weak var txt6: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txt1.delegate = self
        self.txt2.delegate = self
        self.txt3.delegate = self
        self.txt4.delegate = self
        self.txt5.delegate = self
        self.txt6.delegate = self
        txt1.becomeFirstResponder()
    }
    
    
    @IBAction func BtnVerifyProceed(_ sender: UIButton) {
//        if isValidate() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
            // Range.length == 1 means,clicking backspace
        if (range.length == 0){
            if textField == txt1 {
                txt2?.becomeFirstResponder()
            }
            if textField == txt2 {
                txt3?.becomeFirstResponder()
            }
            if textField == txt3 {
                txt4?.becomeFirstResponder()
            }
            if textField == txt4 {
                txt5?.becomeFirstResponder()
                
                if textField == txt5 {
                    txt6?.becomeFirstResponder()
                }
            /*After the otpbox4 is filled we capture the All the OTP textField and do the server call. If you want to capture the otpbox4 use string.*/
                _ = "\((txt1?.text)!)\((txt2?.text)!)\((txt3?.text)!)\((txt4?.text)!)\((txt5?.text)!)\((txt6?.text)!)\(string)"
            }
            textField.text? = string
            return false
        }else if (range.length == 1) {
            
            
            if textField == txt6 {
                txt5?.becomeFirstResponder()
            }
            
            if textField == txt5 {
                txt4?.becomeFirstResponder()
            }

                if textField == txt4 {
                    txt3?.becomeFirstResponder()
                }
                if textField == txt3 {
                    txt2?.becomeFirstResponder()
                }
                if textField == txt2 {
                    txt2?.becomeFirstResponder()
                }
                if textField == txt1 {
                    txt1?.resignFirstResponder()
                }
                textField.text? = ""
                return false
        }
        return true
        }


    
    func isValidate() -> Bool {


        if (txt1.text?.isEmpty)! {
            self.showOkAlertWithHandler("Please enter otp") {
            }
            return false
        }
        else if (txt2.text?.isEmpty)! {
            self.showOkAlertWithHandler("Please enter otp") {
            }
            return false
        }
        else if (txt3.text?.isEmpty)! {
            self.showOkAlertWithHandler("Please enter otp") {
            }
            return false
        }
        else if (txt4.text?.isEmpty)! {
            self.showOkAlertWithHandler("Please enter otp") {
            }
            return false
        }
        else if (txt5.text?.isEmpty)! {
            self.showOkAlertWithHandler("Please enter otp") {
            }
            return false
        }
        else if (txt6.text?.isEmpty)! {
            self.showOkAlertWithHandler("Please enter otp") {
            }
            return false
        }



        return true
    }

    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txt1.resignFirstResponder()
        txt2.resignFirstResponder()
        txt3.resignFirstResponder()
        txt4.resignFirstResponder()
        txt5.resignFirstResponder()
        txt6.resignFirstResponder()
        return true
    }
    
    
    
    
    
    @IBAction func BtnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
}
