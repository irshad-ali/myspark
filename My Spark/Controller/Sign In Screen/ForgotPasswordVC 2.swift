//
//  ForgotPasswordVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 07/12/21.
//

import UIKit

class ForgotPasswordVC: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var TxtEnail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TxtEnail.delegate = self

        // Do any additional setup after loading the view.
    }
    

    @IBAction func BtnSendVerication(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotpasswordSendVerificationEmailVc") as! ForgotpasswordSendVerificationEmailVc
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    func validationOfTextFields() -> Bool{
        if TxtEnail.text == "" {
            self.showOkAlertWithHandler(AlertMessages.Enterregisteredemail.rawValue) {
            }
            return false
        }
        return true
    }
    
    
    @IBAction func BtnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        TxtEnail.resignFirstResponder()
        return true
    }
    
}
