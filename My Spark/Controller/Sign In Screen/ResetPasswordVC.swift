//
//  ResetPasswordVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 07/12/21.
//

import UIKit

class ResetPasswordVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var viewnewpassword: UIView!
    
    @IBOutlet weak var viewconfromepassword: UIView!
    
    @IBOutlet var PopopView: UIView!
   
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var txtConfirom: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewnewpassword.layer.cornerRadius = 20
        viewnewpassword.layer.borderWidth = 0.5
        viewnewpassword.layer.borderColor = UIColor.darkGray.cgColor
        viewnewpassword.clipsToBounds = true
        
        
        viewconfromepassword.layer.cornerRadius = 20
        viewconfromepassword.layer.borderWidth = 0.5
        viewconfromepassword.layer.borderColor = UIColor.darkGray.cgColor
        viewconfromepassword.clipsToBounds = true
        
        
        PopopView.frame = self.view .frame
        self.view.addSubview(self.PopopView)
        PopopView.isHidden = true
        txtPassword.delegate = self
        txtConfirom.delegate = self
       
    }
    
    @IBAction func BtnResetPassword(_ sender: UIButton) {
        if  validationOfTextFields() == true{
        
        self.PopopView.isHidden = false
    }
    }
    
    @IBAction func BtnOk(_ sender: UIButton) {
        
        let vc = self .storyboard?.instantiateViewController(withIdentifier: "TabBarVController") as! TabBarVController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    
    //---------> Validation
    func validationOfTextFields() -> Bool{
        
        if txtPassword.text == "" {
            self.showOkAlertWithHandler(AlertMessages.newPassword.rawValue) {
            }
            return false
        }
        else if (txtPassword?.text?.count)! < 6 {
            self.showOkAlertWithHandler(AlertMessages.passwordcount.rawValue) {
            }
            return false
        }


        else if txtConfirom.text == "" {
            self.showOkAlertWithHandler(AlertMessages.confirmPassword.rawValue) {
            }
            return false
        }

        else if (txtConfirom?.text?.count)! < 6 {
            self.showOkAlertWithHandler(AlertMessages.passwordcount.rawValue) {
            }
            return false
        }

        else if txtPassword.text != txtConfirom.text {

            self.showOkAlertWithHandler(AlertMessages.passwordNotMatch.rawValue) {
            }
            return false

        }

        return true
    }


    
        
    @IBAction func Btnback(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtPassword.resignFirstResponder()
        txtConfirom.resignFirstResponder()
        return true
    }
}
