//
//  SignInVC.swift
//  My Spark
//
//  Created by YATIN  KALRA on 07/12/21.
//

import UIKit

class SignInVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var TxtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmail.delegate = self
        TxtPassword.delegate = self

    }
    
    @IBAction func BtnForgotPassword(_ sender: UIButton) {
 
            
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    
    @IBAction func BtnSignIn(_ sender: UIButton) {
//        if validationOfTextFields() == true {
            
            let vc = self .storyboard?.instantiateViewController(withIdentifier: "TabBarVController") as! TabBarVController
            self.navigationController?.pushViewController(vc, animated: true)
            
//        }
    }
    
    func validationOfTextFields() -> Bool{
        
        if txtEmail.text == "" {
            self.showOkAlertWithHandler(AlertMessages.Enterregisteredemail.rawValue) {
            }
            return false
        }
        
        else if TxtPassword.text == "" {
            self.showOkAlertWithHandler(AlertMessages.enterPassword.rawValue) {
            }
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtEmail.resignFirstResponder()
        TxtPassword.resignFirstResponder()
        return true
    }
 
    @IBAction func BtnSignUp(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpVc") as! SignUpVc
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
