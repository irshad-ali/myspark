//
//  Constant.swift
//  ServiceHubConnect
//
//  Created by Rahul on 16/02/21.
//

import UIKit

let KScreenSize = UIScreen.main.bounds.size
@available(iOS 13.0, *)
let KAppDelegate = (UIApplication.shared.delegate as! AppDelegate)

//MARK: Constant String
struct KString {
    static let strAppHeader              = "ServiceHubConnect !!"
    static let UnderDevelopment          = "Under Development"
    static let layerName                 = "GradientLayer"
    static let TryLater                  = "Try later"
    static let ok                        = "OK"
    static let cancel                    = "Cancel"
}

//MARK: Constant Color
struct KColor {
    static let hexStrAppTheme            = "2cbbab"
    static let hexStrBorder              = "b7b7b7"
}

//MARK: Font
struct KFont {
    static let regular              = "Roboto-Regular"
    static let medium               = "Roboto-Medium"
    static let bold                 = "Roboto-Bold"
}
struct KAlertMsg {
    static let otpMsg = "Enter all field"
}

