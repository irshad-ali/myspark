//
//  ExtensionUtils.swift
//  ServiceHubConnect
//
//  Created by Rahul on 16/02/21.
//

import UIKit

class ExtensionUtils: NSObject {}

//MARK: Extension UIColor
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
       var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
    public class var KColorAppTheme: UIColor{
        return UIColor(hex: KColor.hexStrAppTheme)
    }
    
    public class var KColorBorder: UIColor{
        return UIColor(hex: KColor.hexStrBorder)
    }
}

//MARK: Extension String
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }

    //MARK: Localization
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeAColl() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}

extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}


//MARK: Extension ScrollView
public extension UIScrollView {
    func addRefreshControl(_ block:@escaping ()->()) {
        self.addSubview(SSRefreshControl().addRefreshControl(block))
    }
}

//MARK: Extension NSDictionary
public extension NSDictionary {
    func toData(jsonPath : String = "data") -> Data{
        let dataDict = self.value(forKeyPath: jsonPath)
        do{
            let dataJson = try JSONSerialization.data(withJSONObject: dataDict ?? ["" : ""], options: .prettyPrinted)
            
            //let hobbysDict = try JSONSerialization.data(withJSONObject: dataDict!, options: .allowFragments) as! [String: AnyObject]
            
            
           // let dataJson = try JSONSerialization.data(withJSONObject: dataDict as Any, options: .prettyPrinted)
            return dataJson
        }catch let error{
            alertMessase(message: error.localizedDescription, okAction: {})
        }
        return Data()
    }
    
    func toCustom(jsonPath: String) -> Data {
        let dataDict = self.value(forKey: jsonPath)
        do {
            let dataJson = try JSONSerialization.data(withJSONObject: dataDict as Any, options: .prettyPrinted)
            return dataJson
        } catch let error {
            alertMessase(message: error.localizedDescription, okAction: {})
        }
        return Data()
    }
}


//MARK: Extension Encodable
extension Encodable {
    var postDictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        let postDict = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
        print("Post Dictionary : \(String(describing: postDict))")
        return postDict
    }
}

public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

//MARK: Custom class for Refresh Scroll
class SSRefreshControl: UIRefreshControl {
    var action : (()->())?
    func addRefreshControl(_ block:@escaping ()->()) -> UIRefreshControl {
        action = block
        self.tintColor = UIColor.black
        self.addTarget(self, action: #selector(SSRefreshControl.loadRefreshing), for: .valueChanged)
        return self
    }
    
    @objc func loadRefreshing() {
        action!()
        self.endRefreshing()
    }
}

//MARK: Text field
extension UITextField {
    func addBottomBorder(){
        let border = CALayer()
        let width = CGFloat(1.5)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UIScrollView {
    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToViewAG(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x:0, y:childStartPoint.y,width: 1,height: self.frame.height), animated: animated)
        }
    }
    
    // Bonus: Scroll to top
    func scrollToTopAG(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }
    
    // Bonus: Scroll to bottom
    func scrollToBottomAG() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }
}

extension UITableViewCell{
    static var identifire: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: identifire, bundle: nil)
    }
}

extension UICollectionViewCell{
    static var identifire: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: identifire, bundle: nil)
    }
}



extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func makeCornerView()
    {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.init(red: 0, green: 86, blue: 88, alpha: 1).cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 15
    }
}

