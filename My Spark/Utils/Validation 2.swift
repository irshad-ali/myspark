//
//  UIKit.swift
//  MLS
//
//  Created by YATIN  KALRA on 11/10/21.
//

import UIKit

//for failure and success results
enum ErrorAlert {
    case success
    case failure
    case error
}

//for success or failure of validation with alert message
enum isValid {
    case success
    case unknown
    case failure(ErrorAlert, AlertMessages)
}

enum Valid{
    case success
    case failure(String)
}

let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
let phoneRegex = "^[6-9]\\d{9}$" // "^[0-9]{6,15}$"   //"^[0-9+]{0,1}+[0-9]{5,16}$"

let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
let passwordRegex = "[A-Z0-9a-z._%@+-]{6,15}"
let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
let idValidation = "[A-Z0-9a-z._%@+-/*]{3,20}"
let idTest = NSPredicate(format: "SELF MATCHES %@", idValidation)
let Enterregisteredemail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let confirmPassword = "[A-Z0-9a-z._%@+-]{6,15}"
let passwordNotMatch = NSPredicate(format: "SELF MATCHES %@", passwordRegex)



enum AlertMessages: String {
    case inValidEmail = "Please a enter valid email id"
    case invalidFirstLetterCaps = "First Letter should be capital"
    case inValidPhone = "Please enter a valid mobile Number"
    case invalidAlphabeticString = "Invalid String"
    case invalidAddress = "enter valid email adddress"
    case inValidPSW = "Password must be atleast 6 characters long"
    case emptyPhone = "Please enter mobile Number"
    case emptyEmail = "Please enter Email Id"
    case enterPassword = "Please enter password"
    case passwordcount = "Password count must be between 6 to 15 characters"
    case validPassword = "Please enter valid password"
    case newPassword = "Please enter new password"
    case confirmPassword = "Please enter confirm password"
    case passwordNotMatch = "Password does not match"
    case countryCode = "Please select a country code"
    case Enterregisteredemail = "Enter registered email"
    case userName = "Please Enter UserName"
    case city = "Please enter city"
    
    // EditProfile
    case edEmptyFirstName = "Please enter Full Name"
    case edEmptyLastName = "Please enter Last Name"
    case edIsValidEmail = " Please enter a valid Email id"
    case edEmptyMobileNo = "Please enter Mobile number."
    case edMobileNo = "Please enter a valid Phone number. "
    case edDeliveryAddress = "Please enter delivery address"
    
    // Alert Message
    case alert = "Alert"
    case deleteAddress = "Do you want to delete an address"
    
}


func isValidEmail(_ testStr:String) -> Bool {
    
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: testStr)
}

func isValidPassword(_ testStr:String) -> Bool {
    let passwordRegex = "[A-Z0-9a-z._%@+-]{6,15}"
    
    return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: testStr)
}



class Validation: NSObject {
    
    public static let shared = Validation()
    //SForgot Password Validation
    func ForgotPassword(_ mobileNumberOrEmail : String?) -> Valid {
        if (mobileNumberOrEmail?.isEmpty)! {
            return .failure(AlertMessages.emptyEmail.rawValue.localized())
        }
        if emailTest.evaluate(with: mobileNumberOrEmail) == false{
            return .failure(AlertMessages.edIsValidEmail.rawValue.localized())
        }
        return .success
    }
    
    // login validation
    func validateLogin(_ mobileNumberOrEmail : String?, _ password: String?) -> Valid {
        if (mobileNumberOrEmail?.isEmpty)! {
            return .failure(AlertMessages.emptyEmail.rawValue.localized())
        }
        if emailTest.evaluate(with: mobileNumberOrEmail) == false{
            return .failure(AlertMessages.edIsValidEmail.rawValue.localized())
        }
        if (password?.trim().isEmpty)! {
            return .failure(AlertMessages.enterPassword.rawValue.localized())
        }
        return .success
    }
    
    // Change Password Validation
    func CreatePassword(_ password: String? , _ confirmPassword : String?) -> Valid {
        if (password?.trim().isEmpty)! {
            return .failure(AlertMessages.newPassword.rawValue.localized())
        }
        if (password?.trim().count)! < 6 {
            return .failure(AlertMessages.passwordcount.rawValue.localized())
        }
        if (confirmPassword?.trim().isEmpty)! {
            return .failure(AlertMessages.confirmPassword.rawValue.localized())
        }
        if !((password)?.trim().isEqual(confirmPassword))! {
            return .failure(AlertMessages.passwordNotMatch.rawValue.localized())
        }
        return .success
    }
    
    //SignUp Validation
    func validateSignUp(_ name : String?, _ lastName : String?, _ emailId : String?, _ mobileNumber : String?, _ password : String?, _ confirmPassword : String?, _ city : String?) -> Valid {
        if (name?.trim().isEmpty)! {
            return .failure(AlertMessages.edEmptyFirstName.rawValue.localized())
        }
        if (lastName?.trim().isEmpty)! {
            return .failure(AlertMessages.edEmptyLastName.rawValue.localized())
        }
        if (mobileNumber?.trim().isEmpty)! {
            return .failure(AlertMessages.emptyPhone.rawValue.localized())
        }
        if phoneTest.evaluate(with: mobileNumber) == false{
            return .failure(AlertMessages.edMobileNo.rawValue.localized())
        }
        if (emailId?.isEmpty)! {
            return .failure(AlertMessages.emptyEmail.rawValue.localized())
        }
        if emailTest.evaluate(with: emailId) == false{
            return .failure(AlertMessages.edIsValidEmail.rawValue.localized())
        }
        if (password?.trim().isEmpty)! {
            return .failure(AlertMessages.enterPassword.rawValue.localized())
        }
        if (password?.trim().count)! < 6 {
            return .failure(AlertMessages.passwordcount.rawValue.localized())
        }
        if (confirmPassword?.trim().isEmpty)! {
            return .failure(AlertMessages.confirmPassword.rawValue.localized())
        }
        if (confirmPassword?.trim().count)! < 6 {
            return .failure(AlertMessages.confirmPassword.rawValue.localized())
        }
        if password != confirmPassword {
            return .failure(AlertMessages.passwordNotMatch.rawValue.localized())
        }
        if (city?.trim().isEmpty)! {
            return .failure(AlertMessages.city.rawValue.localized())
        }
        return .success
    }
    
    //Profile Validation
    func validateProfile(_ name : String?, _ emailId : String?, _ mobileNumber : String?) -> Valid {
        
        if (name?.trim().isEmpty)! {
            return .failure(AlertMessages.edEmptyFirstName.rawValue.localized())
        }
        if (mobileNumber?.trim().isEmpty)! {
            return .failure(AlertMessages.emptyPhone.rawValue.localized())
        }
        if phoneTest.evaluate(with: mobileNumber) == true{
            return .failure(AlertMessages.edMobileNo.rawValue.localized())
        }
        if (emailId?.isEmpty)! {
            return .failure(AlertMessages.emptyEmail.rawValue.localized())
        }
        //        if emailTest.evaluate(with: emailId) == false{
        //            return .failure(AlertMessages.edIsValidEmail.rawValue.localized())
        //        }
        return .success
    }
}

enum SwiftAlertType: Int {
    case error
    case info
    case success
    case otp
}

enum DismissAlert : String{
    case success = "Success"
    case oops = "Oops"
    case login = "Login Successfull"
    case locationPermission = "Location Permissions"
    case ok = "Ok"
    case cancel = "Cancel"
    case error = "Error"
    case info = "Warning"
    case confirmation = "Confirmation"
    
    func showWithType(type : DismissAlert? = .info, message : String) {
        alertMessase(message: message) {}
    }
}


extension UIImageView
{
    func makeRoundImage()
    {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    
    
}
